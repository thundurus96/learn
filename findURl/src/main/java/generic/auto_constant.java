package generic;

public interface auto_constant {

	String chromeKey = "webdriver.chrome.driver";
	String chromeValue = "./software/chromedriver.exe";
	String url = "https://www.bushwacker.com/";
	String extentSave = "./genReport.html";
	String readPath = "./src/main/resources/excelSourceFiles/urlsFile.xlsx";
	String sheetName = "allUrls";
	String setPath = "./src/main/resources/excelSourceFiles/allUrls.xlsx";
	
}
