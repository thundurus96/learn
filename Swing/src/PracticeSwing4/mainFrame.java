package PracticeSwing4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class mainFrame extends JFrame{
    private JTextArea textArea;
    private JButton btn;
    private TextPanel textPanel;
    public mainFrame(){
        super("Login");
        //Creating the Object
        textPanel = new TextPanel();
        textArea = new JTextArea();
        btn = new JButton("Click");

        //Settings
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.append("Hello!\n");
            }
        });

        //Add Components to Frame
//        add(textArea, BorderLayout.CENTER);
        add(btn,BorderLayout.SOUTH);
        add(textPanel,BorderLayout.CENTER);

        //Frame Settings
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        setVisible(true);
    }
}
