package PracticeSwing4;

import javax.swing.*;
import java.awt.*;

public class TextPanel extends JPanel {
    private JTextArea textArea;
    public TextPanel(){
        //Creating Object
        textArea = new JTextArea();

        //Settings
        setLayout(new BorderLayout());

        //Adding to Frame
        add(new JScrollPane(textArea),BorderLayout.CENTER);
    }
}
