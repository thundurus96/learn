package PracticeSwing4;

import javax.swing.*;

public class BasicFrame {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new mainFrame();
            }
        });
    }
}
