package Swing3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class borderandtext extends JFrame {

    private JTextArea textArea;
    private JButton btn;

    public borderandtext(){
        super("Login");

        //Settings of TextArea and Button
        textArea = new JTextArea();
        btn = new JButton("OK");

        //Adding TextArea and Button
        add(textArea, BorderLayout.CENTER);
        add(btn,BorderLayout.SOUTH);

        //Listening to Button
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent eve) {
                textArea.append("Hello\n");
            }
        });

                //Frame Settings
                setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
