package Swing4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private JTextArea textArea;
    private TextPanel textPanel;
    private JButton btn;

    public MainFrame(){
        super("Login");

        //Settings of TextArea and Button
        textPanel = new TextPanel();
        btn = new JButton("OK");

        //Adding TextArea and Button
        add(textPanel, BorderLayout.CENTER);
        add(btn,BorderLayout.SOUTH);

        //Listening to Button
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent eve) {
                textPanel.textAppend("Hello\n");
            }
        });

        //Frame Settings
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
