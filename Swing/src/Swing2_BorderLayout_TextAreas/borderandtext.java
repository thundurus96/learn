package Swing2_BorderLayout_TextAreas;

import javax.swing.*;
import java.awt.*;

public class borderandtext extends JFrame {

    private JTextArea textArea;
    private JButton btn;

    public borderandtext(){
        super("Login");

        //Settings of TextArea and Button
        textArea = new JTextArea();
        btn = new JButton("OK");

        //Adding TextArea and Button
        add(textArea, BorderLayout.CENTER);
        add(btn,BorderLayout.SOUTH);

        //Frame Settings
        setSize(400,400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
